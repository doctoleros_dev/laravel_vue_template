import Vue from 'vue'
import router from './router'
import store from './store'

import AppMain from './views/AppMain';

require("./bootstrap");

/* Vue Swal Alert Notification */
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
Vue.use(VueSweetalert2);

Vue.component("app-main", AppMain);

const app = new Vue({
    el: "#app",
    store,
    router,
});
