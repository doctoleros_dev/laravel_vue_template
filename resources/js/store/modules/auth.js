import router from "../../router";
import api from "../../api/auth";

const state = {
    token: localStorage.getItem("access_token"),
    current_user: null,
};

const actions = {
    async login({ commit }, payload) {
        const { data } = await api.login(payload);
        localStorage.setItem("access_token", data.data.token);
        commit("SET_TOKEN", data.data.token);
        router.push({ name: "home" });
    },

    async current_user({ commit },payload) {
        const { data } = await api.current_user(payload);
        commit("SET_CURRENT_USER", data.data);
    },

    logout: ({ commit }) => {
        localStorage.removeItem("access_token");
        commit("SET_TOKEN", null);
        router.push({ name: "login" });
    }
};

const mutations = {
    SET_TOKEN(state, token) {
        state.token = token;
    },
    SET_CURRENT_USER(state, user){
        state.current_user = user;
    }
};

const getters = {
    isLoggedIn() {
        return !!state.token;
    },
    current_user_name(){
        return state.current_user ? state.current_user.name : '';
    }
};

const auth = {
    state,
    mutations,
    actions,
    getters
};

export default auth;
