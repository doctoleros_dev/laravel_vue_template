import router from "../../router";
import api from "../../api/user";

const state = {
    users: null,
    selected_user: null
};

const actions = {
    async index({ commit }, payload) {
        const { data } = await api.index(payload);
        console.log(data);
    },

    async store({ commit }, payload) {
        const { data } = await api.store(payload);
    },

    async show({ commit }, payload) {
        const { data } = await api.show(payload);
        commit('SET_SELECTED_ITEM', data.data)
    },

    async update({ commit }, payload) {
        const { data } = await api.put(payload);
    },

    async destroy({ commit }, payload) {
        const { data } = await api.destroy(payload);
    },
};

const mutations = {
    SET_USERS(state, token) {
        state.token = token;
    },
    SET_SELECTED_ITEM(state, user){
        state.selected_user = user;
    }
};

const getters = {
    isLoggedIn() {
        return !!state.token;
    }
};

const namespaced = {
    namespaced: true,
}

const auth = {
    namespaced,
    state,
    mutations,
    actions,
    getters
};

export default auth;
