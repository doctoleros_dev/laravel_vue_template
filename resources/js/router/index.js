import Vue from "vue";
import VueRouter from "vue-router";
import store from '../store';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    linkActiveClass: "active-one",
    linkExactActiveClass: "active",
    routes: [
        {
            path: "/",
            name: "home",
            component:  () => import(/* webpackChunkName: 'Home' */ '../views/Home'),
            meta: {
                requiresAuth: true,
                permission: 'dashboard'
            }
        },
        {
            path: "/login",
            name: "login",
            component:  () => import(/* webpackChunkName: 'Login' */ '../views/Login'),
            meta: {
                requiresAuth: false,
            }
        },

        /* Settings */
        {
            path: "/users",
            name: "users",
            component:  () => import(/* webpackChunkName: 'Login' */ '../views/settings/users/Users'),
            meta: {
                requiresAuth: true,
            }
        },

        {
            path: "/roles",
            name: "roles",
            component:  () => import(/* webpackChunkName: 'Login' */ '../views/settings/users/Roles'),
            meta: {
                requiresAuth: true,
            }
        },

        {
            path: "/permissions",
            name: "permissions",
            component:  () => import(/* webpackChunkName: 'Login' */ '../views/settings/users/Permissions'),
            meta: {
                requiresAuth: true,
            }
        },

        {
            path: "*",
            name: "notFound",
            component:  () => import(/* webpackChunkName: 'NotFound' */ '../views/errors/NotFound'),
            meta: {
                requiresAuth: true,
            }
        },

    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters.isLoggedIn) {
            next({name: 'login'});
        } else {
            next();
        }
        
    } else {
        next();
    }
});

export default router;
