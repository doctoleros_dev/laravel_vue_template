import axios from './axios';

export default {
    index(payload){
        return axios.get('/users',payload);
    },

    show(id){
        return axios.get(`/users/${id}`);
    },

    store(payload){
        return axios.post('/users');
    },

    update(id, payload){
        return axios.put(`/users/${id}`,payload);
    },

    destroy(id){
        return axios.delete(`/users/${id}`,payload);
    }
}