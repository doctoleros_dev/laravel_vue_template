import axios from './axios';

export default {
    login(payload){
        return axios.post('/login',payload);
    },

    logout(){
        return axios.post(`/logout`);
    },

    register(){

    },

    current_user(payload){
        return axios.get(`/current_user`);
    }
}