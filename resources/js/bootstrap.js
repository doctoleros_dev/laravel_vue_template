import "bootstrap/dist/css/bootstrap.css";
import "./css/custom.scss";

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
    require('admin-lte'); 
} catch (e) {}

