<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>



    <!-- Styles -->
    <style>
        html {
            height: 100%;
        }

        body {
            min-height: 100vh;
        }

        /* fixed and fluid only on sm and up */
        @media (min-width: 768px) {
            .fixed {
                flex: 0 0 200px;
                min-height: 100vh;
                min-width: 180px;
            }

            .col .fluid {
                min-height: 100vh;
            }

            .flex-grow {
                flex: 1;
            }
        }
    </style>
</head>

<body>
    <div id="app">
        <app-main />
    </div>

    <script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
</body>

</html>