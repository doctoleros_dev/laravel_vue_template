<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {          
            $user = Auth::user();

            $token = $user->createToken('Laravel Vue Spa')->accessToken;

            $response = [
                'token' => $token,
                'user' => $user,
                'message' => "Login Successfully"
            ];

            return resolveResponse('Login successfull.', $response, 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);
        }

        $data = $request->all();

        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);
        $success['token'] =  $user->createToken('AppName')->accessToken;

        return response()->json(['success' => $success], 200);
    }
    
    public function logout(){
        $user = Auth::user();
        $user->token()->revoke();
        return resolveResponse('Logout successfull.', null, 200);
    }

    public function current_user(){
        return resolveResponse('Current User', Auth::user(), 200);
    }

}
