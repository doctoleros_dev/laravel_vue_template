<?php

function resolveResponse($message, $data, $code, $request = null) {
    if($request) {
        // CHECK IF ALL OR PAGINATED
        // ALL OR ARRAY RESPONSE
        if($request->has('all')) {
            return response()->json(
                [
                    "message" => $message,
                    "data" => $data->get()
                ],
            $code);
        } 
        // barangay
        elseif($request->has('selected_barangay')) {
            return response()->json(
                [
                    "message" => $message,
                    "data" => $data->get()
                ],
            $code);
        } 
        // PAGINATED
        else {
            $limit = $request->has('limit') ? $request->limit : 15;
            return response()->json(
                [
                    "message" => $message,
                    "data" => $data->paginate($limit)
                ],
            $code);
        }
    } else {
        return response()->json(
            [
                "message" => $message,
                "data" => $data
            ],
        $code);
    }
}

function rejectResponse($message, $data, $code) {
    return response()->json(
        [
            "message" => $message,
            "data" => $data
        ],
    $code);
}

function notAuthorizedResponse($message, $code) {
    return response()->json(
        [
            "message" => $message,
            'code' => $code
        ],
    $code);
}

function maintenanceResponse($message, $code) {
    return response()->json(
        [
            "message" => $message,
            'code' => $code
        ],
    $code);
}
