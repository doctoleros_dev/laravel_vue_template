<?php

use App\Services\AuditLogService;

function transactionHelper($model, $params, $action, $success_message, $error_message, $query_by = null, $query_value = null, $with = []) {
    \DB::beginTransaction();
    try {
        $transaction = null;
        // SWITCH THROUGH ACTIONS
        switch($action) {
            case "CREATE" : {
                $transaction = $model->create($params);
                $transaction = $model->where('id', $transaction->id)
                ->with($with)
                ->first();
                break;
            } 
            case "UPDATE" : {
                $update = $model->where($query_by, $query_value)->first();
                $transaction = $update->update($params);
                break;
            } 
            case "ARCHIVE" : {
                $archive = $model->where($query_by, $query_value)->first();
                $transaction = $archive->delete();
                break;
            } 
            case "RESTORE" : {
                $archive = $model->onlyTrashed()->where($query_by, $query_value)->first();
                $transaction = $archive->restore();
                break;
            } 
        }

        \DB::commit();
        return resolveResponse($success_message, $transaction, 200);
    } catch(\Throwable $e) {
        \DB::rollback();
        return rejectResponse($error_message, $e->getMessage(), 500);
    } catch(\Exception $e) {
        \DB::rollback();
        AuditLogService::logError(\Auth::user() ? \Auth::user()->id : 0, null, $e->getMessage(), class_basename($model), 'ERROR', 'FAILED');
        return rejectResponse($error_message, $e->getMessage(), 500);
    }
}