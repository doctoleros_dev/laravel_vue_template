(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["Home"],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=template&id=63cd6604&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Home.vue?vue&type=template&id=63cd6604& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("div", { staticClass: "card text-center" }, [
        _c("div", { staticClass: "card-header" }, [
          _c(
            "ul",
            {
              staticClass: "nav nav-tabs align-items-end card-header-tabs w-100"
            },
            [
              _c("li", { staticClass: "nav-item" }, [
                _c(
                  "a",
                  {
                    staticClass: "nav-link active",
                    attrs: { href: "http://ftg.test/fields" }
                  },
                  [
                    _c("i", { staticClass: "fa fa-users mr-2" }),
                    _vm._v("Users List")
                  ]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "nav-item" }, [
                _c(
                  "a",
                  {
                    staticClass: "nav-link",
                    attrs: { href: "http://ftg.test/fields/create" }
                  },
                  [
                    _c("i", { staticClass: "fa fa-plus mr-2" }),
                    _vm._v("Create User")
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "ml-auto d-inline-flex" }, [
                _c("li", { staticClass: "nav-item dropdown" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link  dropdown-toggle pt-1",
                      attrs: {
                        "data-toggle": "dropdown",
                        href: "#",
                        role: "button",
                        "aria-haspopup": "true",
                        "aria-expanded": "false"
                      }
                    },
                    [
                      _c("i", { staticClass: "fa fa-save" }),
                      _vm._v(" Export\n                        ")
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "dropdown-menu",
                      staticStyle: {
                        position: "absolute",
                        transform: "translate3d(0px, 34px, 0px)",
                        top: "0px",
                        left: "0px",
                        "will-change": "transform"
                      },
                      attrs: { "x-placement": "bottom-start" }
                    },
                    [
                      _c(
                        "a",
                        {
                          staticClass: "dropdown-item",
                          attrs: { id: "exportCsvDatatable", href: "#" }
                        },
                        [
                          _c("i", { staticClass: "fa fa-file-excel-o mr-2" }),
                          _vm._v("CSV")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "dropdown-item",
                          attrs: { id: "exportExcelDatatable", href: "#" }
                        },
                        [
                          _c("i", { staticClass: "fa fa-file-excel-o mr-2" }),
                          _vm._v("Excel")
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "dropdown-divider" }),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "dropdown-item",
                          attrs: { id: "exportPdfDatatable", href: "#" }
                        },
                        [
                          _c("i", { staticClass: "fa fa-file-pdf-o mr-2" }),
                          _vm._v("PDF")
                        ]
                      )
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link pt-1",
                      attrs: { id: "refreshDatatable", href: "#" }
                    },
                    [
                      _c("i", { staticClass: "fa fa-refresh" }),
                      _vm._v(" Refresh")
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link pt-1",
                      attrs: { id: "printDatatable", href: "#" }
                    },
                    [_c("i", { staticClass: "fa fa-print" }), _vm._v(" Print")]
                  )
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "nav-item" }, [
                  _c(
                    "a",
                    {
                      staticClass: "nav-link pt-1",
                      attrs: { id: "resetDatatable", href: "#" }
                    },
                    [_c("i", { staticClass: "fa fa-undo" }), _vm._v(" Reset")]
                  )
                ]),
                _vm._v(" "),
                _c(
                  "li",
                  {
                    staticClass: "nav-item dropdown keepopen",
                    attrs: { id: "colVisDatatable" }
                  },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "nav-link dropdown-toggle pt-1",
                        attrs: {
                          "data-toggle": "dropdown",
                          href: "#",
                          role: "button",
                          "aria-haspopup": "true",
                          "aria-expanded": "false"
                        }
                      },
                      [
                        _c("i", { staticClass: "fa fa-eye" }),
                        _vm._v(" Columns\n                        ")
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "dropdown-menu" }, [
                      _c(
                        "a",
                        {
                          staticClass: "dropdown-item text-bold",
                          attrs: { href: "#", "data-column": "0" }
                        },
                        [
                          _c("i", { staticClass: "fa fa-check mr-2" }),
                          _vm._v("Name")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "dropdown-item text-bold",
                          attrs: { href: "#", "data-column": "1" }
                        },
                        [
                          _c("i", { staticClass: "fa fa-check mr-2" }),
                          _vm._v("Image")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "dropdown-item text-bold",
                          attrs: { href: "#", "data-column": "2" }
                        },
                        [
                          _c("i", { staticClass: "fa fa-check mr-2" }),
                          _vm._v("Markets")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "dropdown-item text-bold",
                          attrs: { href: "#", "data-column": "3" }
                        },
                        [
                          _c("i", { staticClass: "fa fa-check mr-2" }),
                          _vm._v("Updated\n                                At")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "dropdown-item text-bold",
                          attrs: { href: "#", "data-column": "4" }
                        },
                        [
                          _c("i", { staticClass: "fa fa-check mr-2" }),
                          _vm._v("Action")
                        ]
                      )
                    ])
                  ]
                )
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("h5", { staticClass: "card-title" }, [
            _vm._v("Special title treatment")
          ]),
          _vm._v(" "),
          _c("p", { staticClass: "card-text" }, [
            _vm._v(
              "\n                With supporting text below as a natural lead-in to\n                additional content.\n            "
            )
          ]),
          _vm._v(" "),
          _c("a", { staticClass: "btn btn-primary", attrs: { href: "#" } }, [
            _vm._v("Go somewhere")
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Home.vue":
/*!*************************************!*\
  !*** ./resources/js/views/Home.vue ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=63cd6604& */ "./resources/js/views/Home.vue?vue&type=template&id=63cd6604&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Home.vue?vue&type=template&id=63cd6604&":
/*!********************************************************************!*\
  !*** ./resources/js/views/Home.vue?vue&type=template&id=63cd6604& ***!
  \********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=template&id=63cd6604& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=template&id=63cd6604&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);